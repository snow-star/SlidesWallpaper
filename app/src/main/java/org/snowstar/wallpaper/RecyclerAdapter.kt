package org.snowstar.wallpaper

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import java.io.File

class RecyclerAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {
    private val items = ArrayList<Pair<Bitmap, String>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bitmap = items[position]

        holder.imgView.setImageBitmap(bitmap.first)
        holder.imgView.setOnClickListener {
            val alertDialog = AlertDialog.Builder(context)
                    .setCancelable(true)
                    .setMessage("删除?")
                    .setPositiveButton("确认", DialogInterface.OnClickListener { _, _ ->
                        File(bitmap.second).delete()
                        items.removeAt(position)
                        notifyDataSetChanged()

                        val notifyIntent = Intent(context, MyWallpaperService::class.java)
                        notifyIntent.action = "org.snowstar.wallpaper.data_change"
                        context.startService(notifyIntent)
                    })
                    .create()

            alertDialog.show()
        }
    }

    fun add(bitmap: Bitmap, path: String) {
        items.add(bitmap to path)

        notifyDataSetChanged()
    }

    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val imgView: ImageView = view.findViewById(R.id.imageView)
    }
}