package org.snowstar.wallpaper

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Paint
import android.graphics.Rect
import android.os.Handler
import android.service.wallpaper.WallpaperService
import android.util.DisplayMetrics
import android.util.Log
import android.view.SurfaceHolder
import android.view.WindowManager
import java.io.File
import java.io.FileInputStream
import java.util.*

class MyWallpaperService() : WallpaperService() {
    private val pics: MutableList<Bitmap> = mutableListOf()

    private val rand = Random()

    private val paint = Paint().apply {
        isAntiAlias = true
        isDither = true
//        isFilterBitmap = true
    }

    private var dim = 0.0

    private var deviceWidth = 0

    private var deviceHeight = 0

    override fun onCreateEngine(): Engine {
        return MyEngine()
    }


    override fun onCreate() {
        // 获取实际屏幕长宽
        // 由于 desiredMinimumWidth 比实际宽度要多, 如果用 desiredMinimumWidth 会导致歪掉
        val displayMetrics = DisplayMetrics()
        val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.defaultDisplay.getRealMetrics(displayMetrics)

        deviceHeight = displayMetrics.heightPixels
        deviceWidth = displayMetrics.widthPixels

        // 设备屏幕长宽比
        dim = deviceHeight.toDouble() / deviceWidth

        loadFile()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            if (intent.action == "org.snowstar.wallpaper.data_change") {
                // 有新增图片, 刷新
                loadFile()
            }
        }
        return super.onStartCommand(intent, flags, startId)
    }

    /**
     * 读取应用目录内所有图片
     */
    private fun loadFile() {
        val externalFilesDir = getExternalFilesDir("pics")
        val files = externalFilesDir.list()

        for (pic in files) {
            val picPath = externalFilesDir.path + "/" + pic
            val file = FileInputStream(File(picPath))

            val bitmap = BitmapFactory.decodeStream(file)

            val picDim: Double = bitmap.height.toDouble() / bitmap.width      // 图像长宽比
            val scale: Double = deviceHeight.toDouble() / bitmap.height    // 缩放比

            val scaledBitmap: Bitmap
            scaledBitmap = if (picDim < dim) {
                Bitmap.createScaledBitmap(bitmap, (bitmap.width * scale).toInt(), deviceHeight, true)
            } else {
                Bitmap.createScaledBitmap(bitmap, deviceWidth, (bitmap.height * scale).toInt(), true)
            }

            pics.add(scaledBitmap)
        }
    }

    inner class MyEngine : WallpaperService.Engine() {
        private val handler: Handler = Handler()

        private var current = 0

        private var xOffset = 0f

        private lateinit var targetRect: Rect

        private val task = Runnable {
            if (!pics.isEmpty())
                current = rand.nextInt(pics.size)
            loop()
        }

        override fun onCreate(surfaceHolder: SurfaceHolder?) {
            targetRect = Rect(0, 0, deviceWidth, deviceHeight)

            desiredMinimumHeight
        }

        override fun onVisibilityChanged(visible: Boolean) {
            if (visible) {
                // 切换时立即刷新屏幕
                handler.postDelayed(task, 100)
            } else handler.removeCallbacks(task)  // 不可见时取消定时器. 节省电量

        }

        override fun onOffsetsChanged(xOffset: Float, yOffset: Float, xOffsetStep: Float, yOffsetStep: Float, xPixelOffset: Int, yPixelOffset: Int) {
            this.xOffset = xOffset
            drawPicture()
        }

        /**
         * 定时循环执行绘制.
         */
        private fun loop() {
            handler.removeCallbacks(task)
            drawPicture()
            handler.postDelayed(task, 5000)
        }

        /**
         * 执行绘制图片
         */
        private fun drawPicture() {
            if (pics.isEmpty()) return

            val c = surfaceHolder.lockCanvas()

            val pic = pics[current]

            Log.d("drawPicture", "pic ${pic.height}, ${pic.width}")

            val picDim: Double = pic.height.toDouble() / pic.width      // 图像长宽比
            if (picDim < dim) {
                // 对于横向长的图片, 首屏绘制中间部分, 横向拓展
                /**
                 * ------------------------
                 * |       |      |       |
                 * |  pic  |device|       |
                 * |       |      |       |
                 * |       |      |       |
                 * ------------------------
                 *         ^--------------^
                 *         计算这部分长度
                 */
                val tmpWidth = ((pic.width + deviceWidth) / 2).toInt()

                /**
                 * ------------------------
                 * |       |      |       |
                 * |  pic  |device|       |
                 * |       |      |       |
                 * |       |      |       |
                 * ------------------------
                 * ^-------^
                 * 计算这部分长度
                 */
                val halfWidth = ((pic.width - deviceWidth) / 2).toInt()

                // 根据当前所在屏计算绘制偏移量(左), 乘以 0.3 使滚动幅度减小
                val start = ((tmpWidth - deviceWidth) * xOffset * 0.3).toInt() + halfWidth
                val end = start + (deviceWidth).toInt()  // 绘制偏移终点(右)

                // 实际裁剪图片区域
                val rect = Rect(start, 0, end, pic.height)

                c.drawBitmap(pic, rect, targetRect, paint)

            } else {
                // 竖向长的图片, 绘制垂直方向中心部分.
                val top = ((pic.height - deviceHeight) / 2).toInt()

                val rect = Rect(0, top, pic.width, top + (deviceHeight).toInt())
                c.drawBitmap(pic, rect, targetRect, paint)
            }

            surfaceHolder.unlockCanvasAndPost(c)
        }
    }
}