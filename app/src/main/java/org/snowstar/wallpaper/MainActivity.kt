package org.snowstar.wallpaper

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class MainActivity : AppCompatActivity() {
    private val OPEN_FILE__CODE = 1

    private lateinit var adapter: RecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { _ ->
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.type = "image/*"

            startActivityForResult(intent, OPEN_FILE__CODE)
        }

        adapter = RecyclerAdapter(this)

        list_view.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        list_view.adapter = adapter

        loadPic()
    }

    /**
     * 读取已经存在的图片显示
     */
    private fun loadPic() {
        val externalFilesDir = getExternalFilesDir("pics")
        val pics = externalFilesDir.list()

        for (pic in pics) {
            val picPath = externalFilesDir.path + "/" + pic
            val file = FileInputStream(File(picPath))

            val bitmap = BitmapFactory.decodeStream(file)

            adapter.add(bitmap, picPath)
        }
    }

    private fun addPic(uri: Uri) {
        // 复制图片到应用目录, 供后台服务使用
        val cursor = contentResolver.query(uri, null, null, null, null)
        if (cursor != null && cursor.moveToFirst()) {
            val name = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))

            val externalFilesDir = getExternalFilesDir("pics")
            val copyPath = externalFilesDir.canonicalPath + "/" + name

            val outputStream = FileOutputStream(File(copyPath))
            var inputStream = contentResolver.openInputStream(uri)

            val tmp = ByteArray(inputStream.available())
            inputStream.read(tmp)
            outputStream.write(tmp)

            // 加入显示
            inputStream = contentResolver.openInputStream(uri)
            val bitmap = BitmapFactory.decodeStream(inputStream)
            adapter.add(bitmap, copyPath)

            outputStream.close()
            inputStream.close()
        }
        cursor.close()

        val notifyIntent = Intent(this, MyWallpaperService::class.java)
        notifyIntent.action = "org.snowstar.wallpaper.data_change"

        startService(notifyIntent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            OPEN_FILE__CODE -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val uri = data.data

                    addPic(uri)
                }
            }
        }
    }
}
